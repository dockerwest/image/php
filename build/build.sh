#!/usr/bin/env sh

debianname=$1
phpversion=$2

DEFAULT_DEBIAN_NAME=bookworm
DEBIAN_NAME=${debianname:-bookworm}
PHP_VERSION=${phpversion:-8.3}
IMG_TAG=$PHP_VERSION
if [[ "$DEFAULT_DEBIAN_NAME" != "$DEBIAN_NAME" ]]; then
    IMG_TAG="$PHP_VERSION-$DEBIAN_NAME"
fi

docker build \
    --build-arg DEBIAN_NAME=$DEBIAN_NAME \
    --build-arg PHP_VERSION=$PHP_VERSION \
    --no-cache \
    --tag dockerwest/php:$PHP_VERSION \
    .
