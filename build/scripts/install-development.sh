#!/bin/bash

set -e

curl -LsS https://packages.blackfire.io/gpg.key -o /usr/share/keyrings/blackfire.asc
echo "deb [signed-by=/usr/share/keyrings/blackfire.asc] http://packages.blackfire.io/debian any main" > /etc/apt/sources.list.d/blackfire.list

apt-get update
#apt-get install -y php${DW_PHP_VERSION}-xdebug php${DW_PHP_VERSION}-tideways git blackfire-php
# no tideways yet for php 8
apt-get install -y php${DW_PHP_VERSION}-xdebug git blackfire-php
apt-get clean -y

# composer stable
curl -sSLo /usr/local/lib/composer \
    "https://getcomposer.org/download/latest-stable/composer.phar"
chmod +x /usr/local/lib/composer

# composer 2 LTS
curl -sSLo /usr/local/lib/composer2 \
    "https://getcomposer.org/download/latest-2.2.x/composer.phar"
chmod +x /usr/local/lib/composer2

# composer 1 EOL
curl -sSLo /usr/local/lib/composer1 \
    "https://getcomposer.org/download/latest-1.x/composer.phar"
chmod +x /usr/local/lib/composer1

(
    cd /usr/lib/php/$(php -i | grep ^extension_dir | sed -e 's/.*\/\([0-9]*\).*/\1/')
    curl -O https://raw.githubusercontent.com/tideways/profiler/master/Tideways.php
    [[ -e Tideways.php ]] || false && true
)

printf "xdebug.mode = develop,debug\nxdebug.discover_client_host = 1\nxdebug.max_nesting_level=400\n" \
    >> /etc/php/${DW_PHP_VERSION}/mods-available/xdebug.ini

if [[ -e /etc/php/${DW_PHP_VERSION}/mods-available/tideways.ini ]]; then
    cp -a /etc/php/${DW_PHP_VERSION}/mods-available/tideways.ini \
        /etc/php/${DW_PHP_VERSION}/mods-available/xhprof.ini

    printf "auto_prepend_file=/usr/share/xhprof/prepend.php\n" \
        >> /etc/php/${DW_PHP_VERSION}/mods-available/xhprof.ini

    printf "tideways.udp_connection=\"tideways:8135\"\ntideways.connection=\"tcp://tideways:9135\"\ntideways.monitor_cli=1\n" \
        >> /etc/php/${DW_PHP_VERSION}/mods-available/tideways.ini
    printf "auto_prepend_file=/usr/share/tideways/prepend.php\n" \
        >> /etc/php/${DW_PHP_VERSION}/mods-available/tideways.ini
fi

if [[ -e /etc/php/${DW_PHP_VERSION}/mods-available/blackfire.ini ]]; then
    sed -e 's#\(blackfire.agent_socket\).*#\1=tcp://blackfire:8707#' \
        -i /etc/php/${DW_PHP_VERSION}/mods-available/blackfire.ini
fi

phpdismod xdebug
phpdismod tideways
phpdismod blackfire
